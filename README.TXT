
Inspector Gadget Plugin
=======================

Those from the 80's will get the reference to the cartoon.
However, in practice, this plugin does a few things things:

 - allows you to see a full string of what is in a field that has variables.
 - allows you to select bamboo variables via triggering an auto complete type thing
 - displays a list of available variables and their data
 - if a user clicks the popup, it will pause in its fade out so the resolved vars can be copied
   note, the popup is still removed, if you rollover and out of the input field again it will disappear
 - passwords will be obscured if the key contains the word 'password'
 - added a 'usages' link beside the vars in the global context to look up usages.

Thats it.

How to use
===========
 - Install it.
 - Go to the plan configuration screens.
 - Type ${ to get an autocomplete box up, pick your variable.
 - When you have a nice string like this:
        -g com.atlassian.labs -a atlassian-bot-killer -j ${bamboo.plugin.builds.jira.trunk.version}
   You should be able to roll over it and get the actual variable details used.


Details
=========

The auto complete functionality is attached to any text input boxes that are used in the area of the .task-config
class in the bamboo plan configurations.

The JSON response used for constructing the variables looks a bit like this:

[
    {
        "val": "${bamboo.plan.project.name}",
        "actualValue": "The World",
        "type": "PLAN",
        "label": "${plan.project.name} : The World : PLAN"
    },
    {
        "val": "${bamboo.plan.variable}",
        "actualValue": "biffy clyro",
        "type": "PLAN",
        "label": "${plan.variable} : biffy clyro : PLAN"
    },
    {
        "val": "${bamboo.bike}",
        "actualValue": "cannondale",
        "type": "GLOBAL",
        "label": "${bike} : cannondale : GLOBAL"
    },
    {
        "val": "${bamboo.fruit}",
        "actualValue": "banana",
        "type": "GLOBAL",
        "label": "${fruit} : banana : GLOBAL"
    },
    {
        "val": "${bamboo.test}",
        "actualValue": "test",
        "type": "GLOBAL",
        "label": "${test} : test : GLOBAL"
    }
]

Note, the structure here, the val and label fields are needed for the jQuery plugin to work correctly.

Limitations
===========

For the attaching of the auto complete information to the fields that are loaded via ajax, i.e. all the fields that
are used for task configuration, a delegated event handler is attached to fire when those are added to the screen,
this then adds the auto complete handler.

This is pretty much a limitation in the sew plugin used, and it needs some work to support the delegated event handling
that would clean this up a lot.

It probably should also actually use the @mentions Atlassian code, but at the time of writing this, that stuff
has not been made available to use yet.

Libraries & Licensing
======================

Uses the triggered auto complete from here: https://github.com/tactivos/jquery-sew
The 'sew' plugin is licensed under the 'dfyw' license, i.e. a do 'whatever' you want license.  Indeed.
Let's call that the equivalent of an Apache license.

The 'sew' autocomplete uses the jquery.caret.js file from here: https://github.com/beviz/jquery-caret-position-getter
This is licensed under the Apache license.

Visual Testing Notes
=====================

Bamboo Version  Firefox     Chrome      Safari      IE9     IE10
    4.4          Good        Good        Good        Good    Good
    4.4.2        Good        Good
    5.0-M2       Good        Good        Good


Known issues
============

This url http://localhost:6990/bamboo/build/admin/create/newPlan.action is firing a request to get the variables,
 but its throwing 405 http code back.
