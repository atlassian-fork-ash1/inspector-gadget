$ = AJS.$;
/**
 * jQuery plugin for getting position of cursor in textarea

 * @license under Apache License.
 * @author Bevis Zhao (i@bevis.me, http://bevis.me)
 */
$(function() {

    var calculator = {
        // key styles
        primaryStyles: ['fontFamily', 'fontSize', 'fontWeight', 'fontVariant', 'fontStyle',
            'paddingLeft', 'paddingTop', 'paddingBottom', 'paddingRight',
            'marginLeft', 'marginTop', 'marginBottom', 'marginRight',
            'borderLeftColor', 'borderTopColor', 'borderBottomColor', 'borderRightColor',
            'borderLeftStyle', 'borderTopStyle', 'borderBottomStyle', 'borderRightStyle',
            'borderLeftWidth', 'borderTopWidth', 'borderBottomWidth', 'borderRightWidth',
            'line-height', 'outline'],

        specificStyle: {
            'word-wrap': 'break-word',
            'overflow-x': 'hidden',
            'overflow-y': 'auto'
        },

        simulator : $('<div id="textarea_simulator"/>').css({
            position: 'absolute',
            top: 0,
            left: 0,
            visibility: 'hidden'
        }).appendTo(document.body),

        toHtml : function(text) {
            return text.replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/\n/g, '<br>')
                .split(' ').join('<span style="white-space:prev-wrap">&nbsp;</span>');
        },
        // calculate position
        getCaretPosition: function() {
            var cal = calculator, self = this, element = self[0], elementOffset = self.offset();

            // IE has easy way to get caret offset position
            if ($.browser.msie) {
                // must get focus first
                element.focus();
                var range = document.selection.createRange();
                $('#hskeywords').val(element.scrollTop);
                return {
                    left: range.boundingLeft - elementOffset.left,
                    top: parseInt(range.boundingTop) - elementOffset.top + element.scrollTop
                        + document.documentElement.scrollTop + parseInt(self.getComputedStyle("fontSize"))
                };
            }
            cal.simulator.empty();
            // clone primary styles to imitate textarea
            $.each(cal.primaryStyles, function(index, styleName) {
                self.cloneStyle(cal.simulator, styleName);
            });

            // caculate width and height
            cal.simulator.css($.extend({
                'width': self.width(),
                'height': self.height()
            }, cal.specificStyle));

            var value = self.val(), cursorPosition = self.getCursorPosition();
            var beforeText = value.substring(0, cursorPosition),
                afterText = value.substring(cursorPosition);

            var before = $('<span class="before"/>').html(cal.toHtml(beforeText)),
                focus = $('<span class="focus"/>'),
                after = $('<span class="after"/>').html(cal.toHtml(afterText));

            cal.simulator.append(before).append(focus).append(after);
            var focusOffset = focus.offset(), simulatorOffset = cal.simulator.offset();
            // alert(focusOffset.left  + ',' +  simulatorOffset.left + ',' + element.scrollLeft);
            return {
                top: focusOffset.top - simulatorOffset.top - element.scrollTop
                    // calculate and add the font height except Firefox
                    + ($.browser.mozilla ? 0 : parseInt(self.getComputedStyle("fontSize"))),
                left: focus[0].offsetLeft -  cal.simulator[0].offsetLeft - element.scrollLeft
            };
        }
    };

    $.fn.extend({
        getComputedStyle: function(styleName) {
            if (this.length == 0) return;
            var thiz = this[0];
            var result = this.css(styleName);
            result = result || ($.browser.msie ?
                thiz.currentStyle[styleName]:
                document.defaultView.getComputedStyle(thiz, null)[styleName]);
            return result;
        },
        // easy clone method
        cloneStyle: function(target, styleName) {
            var styleVal = this.getComputedStyle(styleName);
            if (!!styleVal) {
                $(target).css(styleName, styleVal);
            }
        },
        cloneAllStyle: function(target, style) {
            var thiz = this[0];
            for (var styleName in thiz.style) {
                var val = thiz.style[styleName];
                typeof val == 'string' || typeof val == 'number'
                    ? this.cloneStyle(target, styleName)
                    : NaN;
            }
        },
        getCursorPosition : function() {
            var thiz = this[0], result = 0;
            if ('selectionStart' in thiz) {
                result = thiz.selectionStart;
            } else if('selection' in document) {
                var range = document.selection.createRange();
                if (parseInt($.browser.version) > 6) {
                    thiz.focus();
                    var length = document.selection.createRange().text.length;
                    range.moveStart('character', - thiz.value.length);
                    result = range.text.length - length;
                } else {
                    var bodyRange = document.body.createTextRange();
                    bodyRange.moveToElementText(thiz);
                    for (; bodyRange.compareEndPoints("StartToStart", range) < 0; result++)
                        bodyRange.moveStart('character', 1);
                    for (var i = 0; i <= result; i ++){
                        if (thiz.value.charAt(i) == '\n')
                            result++;
                    }
                    var enterCount = thiz.value.split('\n').length - 1;
                    result -= enterCount;
                    return result;
                }
            }
            return result;
        },
        getCaretPosition: calculator.getCaretPosition
    });
});

/**
 * jQuery plugin for getting position of cursor in textarea

 * @license under dfyw (do the fuck you want)
 * @author leChantaux (@leChantaux)
 */

;(function($, window, undefined) {
    // Create the defaults once
    var elementFactory = function(element, value) {
        element.text(value.val);
    };

    var pluginName = 'sew',
        document = window.document,
        defaults = {token: '@', elementFactory: elementFactory};

    function Plugin(element, options) {
        this.element = element;
        this.$element = $(element);
        this.$itemList = $(Plugin.MENU_TEMPLATE);

        this.options = $.extend({}, defaults, options);
        this.reset();

        this._defaults = defaults;
        this._name = pluginName;

        this.expression = new RegExp('(?:^|\\b|\\s)' + this.options.token + '([\\w.]*)$');
        this.cleanupHandle = null;

        this.init();
    }

    Plugin.MENU_TEMPLATE = "<div class='-sew-list-container' style='display: none; position: absolute;'><ul class='-sew-list'></ul></div>";

    Plugin.ITEM_TEMPLATE = '<li class="-sew-list-item"></li>';

    Plugin.KEYS = [40, 38, 13, 27];

    Plugin.prototype.init = function() {
        this.$element.bind('keyup', this.onKeyUp.bind(this))
            .bind('keydown', this.onKeyDown.bind(this))
            .bind('focus', this.renderElements.bind(this, this.options.values))
            .bind('blur', this.remove.bind(this));
    };

    Plugin.prototype.reset = function() {
        this.index = 0;
        this.matched = false;
        this.dontFilter = false;
        this.lastFilter = undefined;
        this.filtered = this.options.values.slice(0);
    };

    Plugin.prototype.next = function() {
        this.index = (this.index + 1) % this.filtered.length;
        this.hightlightItem();
    };

    Plugin.prototype.prev = function() {
        this.index = (this.index + this.filtered.length - 1) % this.filtered.length;
        this.hightlightItem();
    };

    Plugin.prototype.select = function() {
        this.replace(this.filtered[this.index].val);
        this.hideList();
    };

    Plugin.prototype.remove = function() {
        this.cleanupHandle = window.setTimeout(function(){
            this.$itemList.remove();
        }.bind(this), 1000);
    };

    // This function defines what gets replaced when selected.
    Plugin.prototype.replace = function(replacement) {
        var startpos = this.getSelectionStart();
        var fullStuff = this.$element.val();
        var val = fullStuff.substring(0, startpos);

        if (startpos > 2) {
            val = val.replace(this.expression, ' ' + replacement);
        } else {
            val = val.replace(this.expression, replacement);
        }
        
        // If it starts with bamboo variable nomenclature, then don't add a space.

 		var finalFight = val + fullStuff.substring(startpos, fullStuff.length);

        this.$element.val(finalFight);
        this.setCursorPosition(val.length+1);
    };

    Plugin.prototype.hightlightItem = function() {
        this.$itemList.find(".-sew-list-item").removeClass("selected");
        this.filtered[this.index].element.addClass("selected");
    };

    Plugin.prototype.renderElements = function(values) {
        $("body").append(this.$itemList);

        var container = this.$itemList.find('ul').empty();
        values.forEach(function(e, i) {
            var $item = $(Plugin.ITEM_TEMPLATE)

            this.options.elementFactory($item, e);

            e.element = $item.appendTo(container)
                .bind('click', this.onItemClick.bind(this, e))
                .bind('mouseover', this.onItemHover.bind(this, i));
        }.bind(this));

        this.index = 0;
        this.hightlightItem();
    };

    Plugin.prototype.displayList = function() {
        if(!this.filtered.length) return;

        this.$itemList.show();
        var element = this.$element;
        var offset = this.$element.offset();
        var pos = element.getCaretPosition();

        this.$itemList.css({
            left: offset.left + pos.left,
            top: offset.top + pos.top
        });
    };

    Plugin.prototype.hideList = function() {
        this.$itemList.hide();
        this.reset();
    };

    Plugin.prototype.filterList = function(val) {
        if(val == this.lastFilter) return;

        this.lastFilter = val;
        this.$itemList.find(".-sew-list-item").remove();

        var vals = this.filtered = this.options.values.filter(function(e) {
            return val == ""
                || e.val.toLowerCase().indexOf(val.toLowerCase()) >= 0
                || e.label.toLowerCase().indexOf(val.toLowerCase()) >= 0;
        });

        if(vals.length) {
            this.renderElements(vals);
            this.$itemList.show();
        } else {
            this.hideList();
        }
    };

    Plugin.prototype.getSelectionStart = function() {
        input = this.$element[0];

        var pos = input.value.length;

        if(typeof(input.selectionStart) != "undefined") {
            pos = input.selectionStart;
        } else if(input.createTextRange) {
            var r = document.selection.createRange().duplicate();
            r.moveEnd('character', input.value.length);
            if(r.text == '') pos = input.value.length;
            pos = input.value.lastIndexOf(r.text);
        }

        return pos;
    };

    Plugin.prototype.setCursorPosition = function(pos) {
        if (this.$element.get(0).setSelectionRange) {
            this.$element.get(0).setSelectionRange(pos, pos);
        } else if (this.$element.get(0).createTextRange) {
            var range = this.$element.get(0).createTextRange();
            range.collapse(true);
            range.moveEnd('character', pos);
            range.moveStart('character', pos);
            range.select();
        }
    }

    Plugin.prototype.onKeyUp = function(e) {
        var startpos = this.getSelectionStart();
        var val = this.$element.val().substring(0, startpos);
        var matches = val.match(this.expression);

        if(!matches && this.matched) {
            this.matched = false;
            this.dontFilter = false;
            this.hideList();
            return;
        }

        if(matches && !this.matched) {
            this.displayList();
            this.lastFilter = "\n";
            this.matched = true;
        }

        if(matches && !this.dontFilter) {
            this.filterList(matches[1]);
        }
    };

    Plugin.prototype.onKeyDown = function(e) {
        var listVisible = this.$itemList.is(":visible");
        if(!listVisible || (Plugin.KEYS.indexOf(e.keyCode) < 0)) return;

        switch(e.keyCode) {
            case 13:
                this.select();
                break;
            case 40:
                this.next();
                break;
            case 38:
                this.prev();
                break;
            case 27:
                this.hideList();
                this.dontFilter = true;
                break;
        }

        e.preventDefault();
    };

    Plugin.prototype.onItemClick = function(element, e) {
        if(this.cleanupHandle)
            window.clearTimeout(this.cleanupHandle);

        this.replace(element.val);
        this.hideList();
    };

    Plugin.prototype.onItemHover = function(index, e) {
        this.index = index;
        this.hightlightItem();
    };

    $.fn[pluginName] = function(options) {
        return this.each(function() {
            if(!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
            }
        });
    }
}(jQuery, window));

(function ($, BAMBOO) {
    BAMBOO.variableInspector = {};

    var autoCompleteData,
        customItemTemplate = "<div><span /><small /></div>",
        elementFactory = function elementFactory(element, e) {
            var template = $(customItemTemplate).find('span')
                .text(e.val).end()
                .find('small')
                .text("  (" + e.label + ")").end();
            element.append(template);
        };

    /**
     * Load all the available bamboo variables and make them available to be used as auto complete data.
     */
    function loadBambooVariables() {
        var planKey = BAMBOO.currentPlan ? BAMBOO.currentPlan.key : "";
        var request = BAMBOO.VariableInspectorLoader.loadVariablesAvailableInPlan(planKey);
        request.done(function(data){
            autoCompleteData = data;
        });
    };

    /**
     * Attach a delegated event handlers so that when the input text fields on the
     * task configuration pages are loaded we can attach the autocomplete functionality.
     */
    function attachDelegatedHandlerToProvideAutocomplete() {
        $(".task-config").on("focus", "input[type=text]", function(event) {
            if (!$(this).hasClass('sewn') && typeof autoCompleteData !== 'undefined' && autoCompleteData.length > 0) {
                $(this).addClass('sewn').sew({values: autoCompleteData, token: '\\${', elementFactory: elementFactory});
            }
        });

        $(".configSection").on("focus", "input[type=text]", function(event) {
            if (!$(this).hasClass('sewn') && typeof autoCompleteData !== 'undefined' && autoCompleteData.length > 0) {
                $(this).addClass('sewn').sew({values: autoCompleteData, token: '\\${', elementFactory: elementFactory});
            }
        });

        $("#updateTask").on("focus", "input[type=text]", function(event) {
            if (!$(this).hasClass('sewn') && typeof autoCompleteData !== 'undefined' && autoCompleteData.length > 0) {
                $(this).addClass('sewn').sew({values: autoCompleteData, token: '\\${', elementFactory: elementFactory});
            }
        });

        $("#createTask").on("focus", "input[type=text]", function(event) {
            if (!$(this).hasClass('sewn') && typeof autoCompleteData !== 'undefined' && autoCompleteData.length > 0) {
                $(this).addClass('sewn').sew({values: autoCompleteData, token: '\\${', elementFactory: elementFactory});
            }
        });
    };

    /**
     * Add an info icon to display the actual value of the variables.
     */
    function addButtonToDisplayResolvedVariables() {
        skate('form-content-container', {
            type: skate.types.CLASS,
            insert: function (container) {
                $(container).find('input:text').each(function () {
                    if (!$(this).next(".variable-info").length) {
                            prepareVariableInformationFor(this);
                    }
                });
            }
        });
    };

    /**
     * Add the variable info icon after the input field.
     * @param element
     */
    function prepareVariableInformationFor(element) {
        var instance = $(element);
        var variableInfoIcon = insertVariableInfoIcon(instance);
        configureVariableInfoDialog(instance, variableInfoIcon);
    };

    /**
     * Configure the variable info dialog with the resolved variable information.
     * @param textInput
     * @param variableInfoIcon
     */
    function configureVariableInfoDialog(textInput, variableInfoIcon) {
        var id = _.uniqueId('variable_info_dialog_');
        function renderDialog(element, trigger, show) {
            var rawVariables = textInput.attr('value');
            var planKey = BAMBOO.currentPlan ? BAMBOO.currentPlan.key : "";
            var request = BAMBOO.VariableInspectorLoader.resolveVariableInformation(rawVariables, planKey);
            request.done(function(resolvedVariables) {
                element.css({"padding":"5px"});
                if (resolvedVariables.value) {
                    element.html(resolvedVariables.value);
                } else {
                    element.html(AJS.I18n.getText("no.content"));
                }
                show();
            });
        }

        AJS.InlineDialog(variableInfoIcon, id, renderDialog);
    };

    /**
     * Insert the actual icon into the DOM after the supplied text input field.
     * @param textInput
     * @returns {*}
     */
    function insertVariableInfoIcon(textInput) {
        var content = $(BAMBOO.Variable.Inspector.resolvedVariableInfo({id: _.uniqueId('variable_info_link_')}));
        content.insertAfter(textInput);
        return content;
    };

    BAMBOO.variableInspector.loadBambooVariables = loadBambooVariables;
    BAMBOO.variableInspector.attachDelegatedHandlerToProvideAutocomplete = attachDelegatedHandlerToProvideAutocomplete;
    BAMBOO.variableInspector.addButtonToDisplayResolvedVariables = addButtonToDisplayResolvedVariables;

}(jQuery, window.BAMBOO = (window.BAMBOO || {})));

AJS.$(document).ready(function() {
    if ($('.task-config').length) {
        BAMBOO.variableInspector.loadBambooVariables();
        BAMBOO.variableInspector.attachDelegatedHandlerToProvideAutocomplete();
        BAMBOO.variableInspector.addButtonToDisplayResolvedVariables();
    }
});
