
$ = AJS.$;

(function ($, BAMBOO) {
    BAMBOO.VariableUsagesLoader = {};

    /**
     * Load the variable usage info for the supplied variable.
     *
     * @param {string} key The key of the variable to get info for.
     * @return {jQuery.Deferred} A deferred that is resolved with the variable information and the JSON response.
     */
    function loadVariable(key) {
        var deferred = jQuery.Deferred(),
            request;

        request = jQuery.ajax({
            contentType: "application/json",
            dataType: "json",
            url: AJS.contextPath() + "/rest/variables/1.0/variables/usages/" + encodeURIComponent(key)
        });

        request.error(function () {
            deferred.reject();
        });

        request.success(function (response) {
            deferred.resolve(response);
        });

        return deferred.promise();
    };

    /**
     * Load the variable usage info for the supplied variable in a specific plan.
     *
     * @param {string} key The key of the variable to get info for.
     * @param {string} plan The plan in which to get the variable info.
     * @return {jQuery.Deferred} A deferred that is resolved with the variable information and the JSON response.
     */
    function loadVariableForPlan(key, plan) {
        var deferred = jQuery.Deferred(),
            request;


        request = jQuery.ajax({
            contentType: "application/json",
            dataType: "json",
            url: AJS.contextPath() + "/rest/variables/1.0/variables/usages/" + plan + "/" + encodeURIComponent(key)
        });

        request.error(function () {
            deferred.reject();
        });

        request.success(function (response) {
            deferred.resolve(response);
        });

        return deferred.promise();
    };

    BAMBOO.VariableUsagesLoader.loadVariable = loadVariable;
    BAMBOO.VariableUsagesLoader.loadVariableForPlan = loadVariableForPlan;

}(jQuery, window.BAMBOO = (window.BAMBOO || {})));