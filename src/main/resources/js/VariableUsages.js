$ = AJS.$;

(function ($, BAMBOO) {
    BAMBOO.variableUsages = {};

    /**
     *  Add a handler to add a surface variables icon to the global variables page.
     */
    function addDisplayGlobalVariableUsageLink() {
        $('#globalVariables .variables-list > tbody > tr').not(':first').each(function(){
            var variableKey = $(this).find('.inline-edit-view').html();
            var operationsCell = $(this).find('.operations');
            var usageLink = jQuery(BAMBOO.Variable.Inspector.variableUsagesLink({variableKey: variableKey})).appendTo(operationsCell);
            usageLink.click(function(event) {
                event.preventDefault();
                var content =  BAMBOO.Variable.Inspector.variableUsagesLoading();
                var dialog = displayUsagesDialog(content, variableKey);
                var request = BAMBOO.VariableUsagesLoader.loadVariable($(this).attr('id'));
                request.done(function(data){
                    planContent = BAMBOO.Variable.Inspector.variableUsages({contextPath: AJS.contextPath(), usages: data});
                    repositoryContent = BAMBOO.Variable.Inspector.variableUsagesInRepositories({contextPath: AJS.contextPath(), usages: data});
                    updateUsagesDialog(planContent, repositoryContent, dialog);
                });

                request.fail(function(data) {
                    displayErrorDialog(data, dialog);
                });
            });
        });
    };

    /**
     * Add a handler to add a surface variables icon to the plan variables page.
     */
    function addDisplayPlanVariableUsageLink() {
        $('#planVariables .variables-list > tbody > tr').not(':first').each(function(){
            var variableKey = $(this).find('.inline-edit-view').html();
            var operationsCell = $(this).find('.operations');
            var usageLink = jQuery(BAMBOO.Variable.Inspector.variableUsagesLink({variableKey: variableKey})).appendTo(operationsCell);
            usageLink.click(function(event) {
                event.preventDefault();
                var content =  BAMBOO.Variable.Inspector.variableUsagesLoading();
                var dialog = displayUsagesDialog(content, variableKey);
                var planKey = BAMBOO.currentPlan ? BAMBOO.currentPlan.key : "";
                var request = BAMBOO.VariableUsagesLoader.loadVariableForPlan($(this).attr('id'), planKey);
                request.done(function(data){
                    planContent = BAMBOO.Variable.Inspector.variableUsages({contextPath: AJS.contextPath(), usages: data});
                    repositoryContent = BAMBOO.Variable.Inspector.variableUsagesInRepositories({contextPath: AJS.contextPath(), usages: data});
                    updateUsagesDialog(planContent, repositoryContent, dialog);
                });

                request.fail(function(data) {
                    displayErrorDialog(data, dialog);
                });
            });
        });
    };

    /**
     * Display the variable usages dialog with the supplied content.
     * @param content The content to render.
     * @param data The original data.
     */
    function displayUsagesDialog(content, data, dialog) {
        dialog = new AJS.Dialog({
            width: 650,
            height: 450,
            id: "variable-usages-dialog",
            closeOnOutsideClick: true
        });

        dialog.addHeader(data + " " + AJS.I18n.getText('variable.usages.header'));
        dialog.addPanel("Plan Use", content, "panel-body");
        dialog.addLink("Close", function (dialog) {dialog.hide();}, "#");
        dialog.gotoPage(0);
        dialog.gotoPanel(0);
        dialog.show();
        return dialog;
    }

    /**
     * Update the content of the dialog.
     * @param content
     * @param dialog
     */
    function updateUsagesDialog(planContent, repositoryContent, dialog) {
        var panel = dialog.getCurrentPanel();
        panel.body.html(planContent);
        dialog.addPanel('Repository Use', repositoryContent, "panel-body");
        dialog.gotoPage(0);
        dialog.gotoPanel(0);
    }

    /**
     * Display an error dialog.
     * @param content The content to render.
     */
    function displayErrorDialog(content, dialog) {
        dialog.addHeader(AJS.I18n.getText('variable.usages.header.error'));
        var panel = dialog.getCurrentPanel();
        panel.body.html(content);
    }

    BAMBOO.variableUsages.addDisplayGlobalVariableUsageLink = addDisplayGlobalVariableUsageLink;
    BAMBOO.variableUsages.addDisplayPlanVariableUsageLink = addDisplayPlanVariableUsageLink;

}(jQuery, window.BAMBOO = (window.BAMBOO || {})));

AJS.$(document).ready(function() {
    BAMBOO.variableUsages.addDisplayGlobalVariableUsageLink();
    BAMBOO.variableUsages.addDisplayPlanVariableUsageLink();
});
