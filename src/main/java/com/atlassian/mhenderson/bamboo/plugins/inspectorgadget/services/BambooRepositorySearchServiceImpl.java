package com.atlassian.mhenderson.bamboo.plugins.inspectorgadget.services;

import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.bamboo.plan.cache.ImmutableTopLevelPlan;
import com.atlassian.bamboo.repository.PlanRepositoryLink;
import com.atlassian.bamboo.repository.RepositoryDefinitionManager;
import com.atlassian.bamboo.vcs.configuration.PlanRepositoryDefinition;
import com.atlassian.bamboo.vcs.configuration.VcsConfigurationFragment;
import com.atlassian.bamboo.vcs.configuration.VcsRepositoryData;
import com.atlassian.mhenderson.bamboo.plugins.inspectorgadget.rest.VariableUsageInRepositoryInformation;
import com.google.common.base.Function;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.atlassian.mhenderson.bamboo.plugins.inspectorgadget.services.BambooRepositorySearchServiceImpl.PlanLinks.convertLinkToKey;
import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.transform;


/**
 * Searches bamboo repositories for usages of variables.
 */
public class BambooRepositorySearchServiceImpl implements BambooRepositorySearchService {

    @SuppressWarnings("unused")
    private static final Logger log = Logger.getLogger(BambooRepositorySearchServiceImpl.class);
    private final RepositoryDefinitionManager repositoryDefinitionManager;
    private final CachedPlanManager planManager;

    /**
     * Construct this searcher to search through bamboo repositories.
     * @param repositoryDefinitionManager
     */
    public BambooRepositorySearchServiceImpl(final RepositoryDefinitionManager repositoryDefinitionManager,
                                             final CachedPlanManager planManager) {
        this.repositoryDefinitionManager = repositoryDefinitionManager;
        this.planManager = planManager;
    }

    /**
     * Finds all usages of a variable in all of the shared repositories, if found then provide information
     * as to which plan it is used in.
     * @param variableKey
     * @return List
     * @throws Exception
     */
    @Override
    public List<VariableUsageInRepositoryInformation> findUsagesOfVariable(final String variableKey) {

        List<VariableUsageInRepositoryInformation> results = new ArrayList<VariableUsageInRepositoryInformation>();
        for (VcsRepositoryData repositoryData : repositoryDefinitionManager.getLinkedRepositories()) {
            results.addAll(checkConfigurationsForUsage(variableKey, repositoryData.getId(), repositoryData.getName(),
                    repositoryData.isLinked(), repositoryData.getVcsBranchDetectionOptions()));
            results.addAll(checkConfigurationsForUsage(variableKey, repositoryData.getId(), repositoryData.getName(),
                    repositoryData.isLinked(), repositoryData.getVcsChangeDetectionOptions()));
            results.addAll(checkConfigurationsForUsage(variableKey, repositoryData.getId(), repositoryData.getName(),
                    repositoryData.isLinked(), repositoryData.getVcsLocation()));
        }

        return results;
    }

    /**
     * Finds all usages of a variable in all of the repositories for all plans, if found then provide information
     * as to which plan it is used in.
     * @param variableKey
     * @return List
     * @throws Exception
     */
    @Override
    public List<VariableUsageInRepositoryInformation> findUsagesOfVariableInPlanRepositories(final String variableKey) {
        List<VariableUsageInRepositoryInformation> results = new ArrayList<VariableUsageInRepositoryInformation>();

        for (ImmutableTopLevelPlan plan :planManager.getPlans()) {
            results.addAll(findUsagesOfVariable(variableKey, plan));
        }

        return results;
    }

    /**
     * Finds all usages of a variable in all of the repositories for a plan, if found then provide information
     * as to which plan it is used in.
     * @param variableKey
     * @return List
     * @throws Exception
     */
    @Override
    public List<VariableUsageInRepositoryInformation> findUsagesOfVariable(final String variableKey,
                                                                           final ImmutablePlan plan) {
        List<VariableUsageInRepositoryInformation> results = new ArrayList<VariableUsageInRepositoryInformation>();

        // Do a run through the vcs configs to find if a variable is used anywhere.
        for (PlanRepositoryDefinition definition : repositoryDefinitionManager.getPlanRepositoryDefinitions(plan)) {
            results.addAll(checkConfigurationsForUsage(variableKey, definition.getId(), definition.getName(),
                    definition.isLinked(), definition.getVcsBranchDetectionOptions()));
            results.addAll(checkConfigurationsForUsage(variableKey, definition.getId(), definition.getName(),
                    definition.isLinked(), definition.getVcsChangeDetectionOptions()));
            results.addAll(checkConfigurationsForUsage(variableKey, definition.getId(), definition.getName(),
                    definition.isLinked(), definition.getVcsLocation()));
        }

        return results;
    }

    private List<VariableUsageInRepositoryInformation> checkConfigurationsForUsage(final String variableKey,
                                                                                   final long repositoryId,
                                                                                   final String repositoryName,
                                                                                   final boolean isGlobal,
                                                                                   final VcsConfigurationFragment fragment) {

        List<VariableUsageInRepositoryInformation> results = new ArrayList<VariableUsageInRepositoryInformation>();
        if (fragment != null && fragment.getConfiguration() != null) {
            Map<String, String> map = fragment.getConfiguration();
            for(String value : map.values()) {
                if (value.contains(variableKey)) {
                    results.add(new VariableUsageInRepositoryInformation(
                            repositoryId, repositoryName,
                            isGlobal, getPlanKeysForRepository(repositoryId)));
                }
            }
        }

        return results;
    }


    private List<String> getPlanKeysForRepository(final long repositoryId) {
        List<PlanRepositoryLink> links = repositoryDefinitionManager.getPlansUsingRepository(repositoryId);
        return copyOf(transform(links, convertLinkToKey()));
    }

    static class PlanLinks{
        public static Function convertLinkToKey() {
            return (Function<PlanRepositoryLink, String>) input -> input.getPlan().getPlanKey().getKey();
        }
    }
}