package com.atlassian.mhenderson.bamboo.plugins.inspectorgadget.services;

import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.plan.cache.ImmutableChainImpl;
import com.atlassian.bamboo.plan.cache.ImmutableJob;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.mhenderson.bamboo.plugins.inspectorgadget.rest.VariableUsageInPlanInformation;
import com.google.common.base.Predicate;
import org.apache.commons.lang.time.StopWatch;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.google.common.collect.Maps.filterValues;
import static com.google.common.collect.Maps.newHashMap;

/**
 * Searches the task definition configuration map for usages of variables.
 */
public class TaskDefinitionBambooPlanSearchService implements BambooPlanSearchService
{
    private static final Logger log = Logger.getLogger(TaskDefinitionBambooPlanSearchService.class);

    private final CachedPlanManager planManager;

    /**
     * Construct this search to search through bamboo plans.
     * @param planManager
     */
    public TaskDefinitionBambooPlanSearchService(final CachedPlanManager planManager) {
        this.planManager = planManager;
    }

    /**
     * Finds all usages of a variable in all of the bamboo plans available.
     * The format returned should be - Used in PLAN: Blah, JOB:Blah, TASK:Call Command, ITEM:
     * @param planAndJobKey
     * @param variableKey
     * @return List
     * @throws Exception
     */
    @Override
    public List<VariableUsageInPlanInformation> findUsagesOfVariableInPlan(final String planAndJobKey, final String variableKey) {
        StopWatch stopwatch = new StopWatch();
        stopwatch.start();

        Predicate<String> taskConfigurationFilter = new Predicate<String>() {
            public boolean apply(String value) {
                return value.contains(variableKey);
            }
        };
        List<VariableUsageInPlanInformation> results = new ArrayList<VariableUsageInPlanInformation>();
        ImmutablePlan plan = planManager.getPlanByKey(extractPlanKeyFromPlanAndJobKey(planAndJobKey));
        logDebug("findUsagesOfVariableInAllPlans - found plan:" + plan);

        if (plan != null && plan instanceof ImmutableChainImpl) {
            logDebug("findUsagesOfVariableInAllPlans - plan has jobs:" + ((ImmutableChainImpl) plan).getAllJobs().size());
            for (ImmutableJob job : ((ImmutableChainImpl) plan).getAllJobs()){
                BuildDefinition definition = job.getBuildDefinition();
                for (TaskDefinition task : definition.getTaskDefinitions()) {
                    results.addAll(findUsageInTask(plan.getName(), plan.getName(), plan.getKey(), task, taskConfigurationFilter));
                }
            }
        }

        stopwatch.stop();
        logDebug("findUsagesOfVariableInAllPlans took:" + stopwatch.getTime());

        return results;
    }

    /**
     * Finds all usages of a variable in all of the bamboo plans available.
     * The format returned should be - Used in PLAN: Blah, JOB:Blah, TASK:Call Command, ITEM:
     * @param variableKey
     * @return List
     * @throws Exception
     */
    @Override
    public List<VariableUsageInPlanInformation> findUsagesOfVariableInAllPlans(final String variableKey) {
        StopWatch stopwatch = new StopWatch();
        stopwatch.start();

        Predicate<String> taskConfigurationFilter = new Predicate<String>() {
            public boolean apply(String value) {
                return value.contains(variableKey);
            }
        };
        List<VariableUsageInPlanInformation> results = new ArrayList<VariableUsageInPlanInformation>();
        List<ImmutablePlan> allPlans = planManager.getPlans(ImmutablePlan.class);
        logDebug("findUsagesOfVariableInAllPlans - loaded:" + allPlans.size() + " plans to search.");
        for (ImmutablePlan plan : allPlans) {
            BuildDefinition definition = plan.getBuildDefinition();
            for (TaskDefinition task : definition.getTaskDefinitions()) {
                results.addAll(findUsageInTask(plan.getName(), plan.getName(), plan.getKey(), task, taskConfigurationFilter));
            }
        }

        stopwatch.stop();
        logDebug("findUsagesOfVariableInAllPlans took:" + stopwatch.getTime());

        return results;
    }

    private List<VariableUsageInPlanInformation> findUsageInTask(String plan, String job, String key, TaskDefinition taskDefinition, Predicate<String> taskConfigurationFilter) {

        List<VariableUsageInPlanInformation> resultList = new ArrayList<VariableUsageInPlanInformation>();
        Map<String, String> filteredResults = newHashMap(filterValues(taskDefinition.getConfiguration(), taskConfigurationFilter));
        for (Map.Entry<String, String> entry: filteredResults.entrySet()) {
            String userDescription = taskDefinition.getUserDescription();
            String itemType = entry.getKey();
            resultList.add(new VariableUsageInPlanInformation(plan, job, key, userDescription, itemType));
        }

        return resultList;
    }

    private void logDebug(String message) {
        if (log.isDebugEnabled()) {
            log.debug(message);
        }
    }

    private PlanKey extractPlanKeyFromPlanAndJobKey(String planAndJobKey) {
        PlanKey fullKey = PlanKeys.getPlanKey(planAndJobKey);
        if (PlanKeys.isJobKey(fullKey)) {
            PlanKey key = PlanKeys.getPlanKey(planAndJobKey);
            return PlanKeys.getChainKeyFromJobKey(key);
        } else {
            return fullKey;
        }
    }
}