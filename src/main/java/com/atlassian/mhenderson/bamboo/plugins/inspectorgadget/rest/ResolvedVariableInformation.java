package com.atlassian.mhenderson.bamboo.plugins.inspectorgadget.rest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * A class that represents the resolved information for a sting containing bamboo variable information.
 * This may be have more than one bamboo variable.  For example:
 * MAVEN_OPTS=Xmx256m ${bamboo.ops} TEST ${bamboo.variable}
 * The value will be the resolved string.
 */
@XmlRootElement(name = "resolved-bamboo-variable")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResolvedVariableInformation
{
    private String value;

    public ResolvedVariableInformation(){}

    public ResolvedVariableInformation(final String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(final String value) {
        this.value = value;
    }
}
