package com.atlassian.mhenderson.bamboo.plugins.inspectorgadget.rest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Simple representation of a bamboo plan or global variable.
 */
@XmlRootElement(name = "bamboo-variable")
@XmlAccessorType(XmlAccessType.FIELD)
public class BambooVariable
{
    private String val;
    private String actualValue;
    private String type;
    private String label;

    public BambooVariable(String val, String actualValue, String type) {
        this.val = "${bamboo." + val + "}";
        this.actualValue = actualValue;
        this.type = type;
        this.label = "${" + val + "} : " + actualValue + " : " + type;
    }

    public String getVal() {
        return val;
    }

    public void setVal(final String val) {
        this.val = val;
    }

    public String getActualValue() {
        return actualValue;
    }

    public void setActualValue(final String actualValue) {
        this.actualValue = actualValue;
    }

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(final String label) {
        this.label = label;
    }
}
