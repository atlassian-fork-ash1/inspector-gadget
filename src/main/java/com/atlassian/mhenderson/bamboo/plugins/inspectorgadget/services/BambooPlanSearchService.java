package com.atlassian.mhenderson.bamboo.plugins.inspectorgadget.services;

import com.atlassian.mhenderson.bamboo.plugins.inspectorgadget.rest.VariableUsageInPlanInformation;

import java.util.List;

/**
 * Defines an interface for searching bamboo plans for information.
 * At present this only provides the facility to search for variable information by key.
 */
public interface BambooPlanSearchService
{
    /**
     * Search all bamboo plans for the variable with the supplied key.
     * @param variableKey
     * @return A String in the format: Used in PLAN: Blah, JOB:Blah, TASK:Call Command, ITEM: Blah
     */
    List<VariableUsageInPlanInformation> findUsagesOfVariableInAllPlans(String variableKey);

    /**
     * Finds all usages of a variable in all of the bamboo plans available.
     * The format returned should be - Used in PLAN: Blah, JOB:Blah, TASK:Call Command, ITEM:
     * @param planAndJobKey
     * @param variableKey
     * @return List
     * @throws Exception
     */
    List<VariableUsageInPlanInformation> findUsagesOfVariableInPlan(final String planAndJobKey, final String variableKey);
}
