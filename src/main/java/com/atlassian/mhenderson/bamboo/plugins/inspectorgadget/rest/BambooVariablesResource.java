package com.atlassian.mhenderson.bamboo.plugins.inspectorgadget.rest;

import com.atlassian.bamboo.variable.VariableDefinition;
import com.atlassian.mhenderson.bamboo.plugins.inspectorgadget.services.BambooPlanSearchService;
import com.atlassian.mhenderson.bamboo.plugins.inspectorgadget.services.BambooRepositorySearchService;
import com.atlassian.mhenderson.bamboo.plugins.inspectorgadget.services.BambooVariableSearchService;
import org.apache.log4j.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A resource for allowing access to the bamboo global variables and the plan
 * level variables.  Note, this provides a very specific format that is designed to be used
 * as part of the auto complete functionality for variables.
 * The format is:
 * <pre>
 * [
 *   {
 *     "val": "${bamboo.plan.project.name}",
 *     "actualValue": "The World",
 *     "type": "PLAN",
 *     "label": "${plan.project.name} : The World : PLAN"
 *   },
 *   {
 *     "val": "${bamboo.plan.variable}",
 *     "actualValue": "biffy clyro",
 *     "type": "PLAN",
 *     "label": "${plan.variable} : biffy clyro : PLAN"
 *   }
 * ]
 * </pre>
 */
@Path("/variables")
public class BambooVariablesResource {

    private static final Logger log = Logger.getLogger(BambooVariablesResource.class);

    private final Pattern variableExtractor = Pattern.compile("\\{(.*?)\\}(?!\\s*\\})\\s*", Pattern.DOTALL);

    private final BambooPlanSearchService planSearcher;
    private final BambooVariableSearchService variableSearchService;
    private final BambooRepositorySearchService repositorySearchService;

    /**
     * Creates this resource with the necessary variable contexts and managers to populate the requests with information.
     */
    public BambooVariablesResource(final BambooPlanSearchService planSearcher,
                                   final BambooVariableSearchService variableSearchService,
                                   final BambooRepositorySearchService repositorySearchService) {
        this.planSearcher = planSearcher;
        this.variableSearchService = variableSearchService;
        this.repositorySearchService = repositorySearchService;
    }

    /**
     * Get links to the usages of a GLOBAL variable.  Note, this looks up variables by key.
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("usages/{variableKey}")
    public Response findUsagesOfGlobalVariableByKey(@PathParam("variableKey") String variableKey) {
        VariableDefinition variable = variableSearchService.lookupGlobalVariableByName(variableKey);
        if (variable != null) {
            List<VariableUsageInPlanInformation> usages = planSearcher.findUsagesOfVariableInAllPlans(variable.getKey());
            Set<VariableUsageInRepositoryInformation> usagesInRepositories = new HashSet<VariableUsageInRepositoryInformation>();
            usagesInRepositories.addAll(repositorySearchService.findUsagesOfVariable(variable.getKey()));
            usagesInRepositories.addAll(repositorySearchService.findUsagesOfVariableInPlanRepositories(variable.getKey()));

            VariableUsageInformation usageInformation = new VariableUsageInformation(variableKey, variable.getKey(), variable.getValue());
            usageInformation.setVariableUsageLocations(usages);
            usageInformation.setVariableUsageInRepositoryLocations(usagesInRepositories);

            return Response.ok(usageInformation).cacheControl(noCache()).build();
        } else {
            return variableNotFoundResponse();
        }
    }

    /**
     * Get links to the usages of a PLAN variable.  Note, this looks up variables by key.
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("usages/{planAndJobKey}/{variableKey}")
    public Response findUsagesOfPlanVariableByKey(@PathParam("planAndJobKey") String planAndJobKey,
                                                  @PathParam("variableKey") String variableKey) {
        VariableDefinition variable = variableSearchService.lookupPlanVariableByName(planAndJobKey, variableKey);
        if (variable != null) {
            List<VariableUsageInPlanInformation> usages = planSearcher.findUsagesOfVariableInPlan(planAndJobKey, variable.getKey());
            List<VariableUsageInRepositoryInformation> usagesInRepositories = repositorySearchService.findUsagesOfVariableInPlanRepositories(variable.getKey());

            VariableUsageInformation usageInformation = new VariableUsageInformation(variableKey, variable.getKey(), variable.getValue());
            usageInformation.setVariableUsageLocations(usages);
            usageInformation.setVariableUsageInRepositoryLocations(usagesInRepositories);
            return Response.ok(usageInformation).cacheControl(noCache()).build();
        } else {
            return variableNotFoundResponse();
        }
    }

    /**
     * Get all global variables that are available.
     * This will return, global.
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getAllVariables() {
        List<VariableDefinition> variableDefinitions = variableSearchService.getAllVariableDefinitions("");
        List<BambooVariable> variables = variableSearchService.prepareFullListOfVariables(variableDefinitions);
        return Response.ok(variables).cacheControl(noCache()).build();
    }

    /**
     * Get all variables that are available for a plan and job key.
     * This will return, global, plan and runtime variables.  The runtime variables are from the previous plan execution.
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("{planAndJobKey}")
    public Response getAllVariables(@PathParam("planAndJobKey") String planAndJobKey) {
        List<VariableDefinition> variableDefinitions = variableSearchService.getAllVariableDefinitions(planAndJobKey);
        List<BambooVariable> variables = variableSearchService.prepareFullListOfVariables(variableDefinitions);
        return Response.ok(variables).cacheControl(noCache()).build();
    }

    /**
     * Get a resolved set of variables using their name.  This will go through each variable supplied in the string
     * and resolve to the correct value.  Plan variables that are named the same as global variables will take precedence.
     */
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_FORM_URLENCODED})
    @Path("/resolve")
    public Response getVariablesByName(@FormParam("planAndJobKey") String planAndJobKey,
                                       @FormParam("keys") String input) {
        String output = input;
        List<String> variableKeys = extractVariableKeys(input);
        List<VariableDefinition> allDefinitions = variableSearchService.getAllVariableDefinitions(planAndJobKey);
        for (String key: variableKeys) {
            output = addVariableValueToResponseAndHidePasswords(
                    output, key, variableSearchService.findVariableByKey(key, allDefinitions));
        }

        logDebug("getVariablesByName - resolved output:" + output);
        return Response.ok(new ResolvedVariableInformation(output)).cacheControl(noCache()).build();
    }

    private CacheControl noCache() {
        return CacheControl.valueOf("no-cache");
    }

    private Response variableNotFoundResponse() {
        return response("The variable could not be found in any plans.", Response.Status.BAD_REQUEST);
    }

    private Response response(String messageCode, Response.Status status) {
        String errorMessage = messageCode;
        return Response.status(status).entity(errorMessage).cacheControl(noCache()).build();
    }

    private String addVariableValueToResponseAndHidePasswords(String response, String key, VariableDefinition definition) {
        return (definition != null)
                ? response.replace("${bamboo." + key + "}", variableSearchService.obscurePasswordForKeyIfNecessary(key, definition.getValue()))
                : response;
    }

    private List<String> extractVariableKeys(String input) {
        Matcher m = variableExtractor.matcher(input);
        List<String> variableKeys = new ArrayList<String>();
        while (m.find()) {
            variableKeys.add(m.group(1).replace("bamboo.", ""));
        }

        logDebug("extractVariableKeys - found variable keys:" + variableKeys);
        return variableKeys;
    }

    private void logDebug(String message) {
        if (log.isDebugEnabled()) {
            log.debug(message);
        }
    }
}