package com.atlassian.mhenderson.bamboo.plugins.inspectorgadget.rest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * A class that contains all the of the places in which a variable is actually used in plans.
 */
@XmlRootElement(name = "bamboo-variable-usage-plan")
@XmlAccessorType(XmlAccessType.FIELD)
public class VariableUsageInPlanInformation
{
    private String plan;
    private String job;
    private String task;
    private String item;
    private String key;

    public VariableUsageInPlanInformation(){}

    public VariableUsageInPlanInformation(final String plan, final String job, final String key, final String task, final String item) {
        this.plan = plan;
        this.job = job;
        this.task = task;
        this.item = item;
        this.key = key;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(final String plan) {
        this.plan = plan;
    }

    public String getJob() {
        return job;
    }

    public void setJob(final String job) {
        this.job = job;
    }

    public String getTask() {
        return task;
    }

    public void setTask(final String task) {
        this.task = task;
    }

    public String getItem() {
        return item;
    }

    public void setItem(final String item) {
        this.item = item;
    }

    public String getKey() {
        return key;
    }

    public void setKey(final String key) {
        this.key = key;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final VariableUsageInPlanInformation that = (VariableUsageInPlanInformation) o;
        if (item != null ? !item.equals(that.item) : that.item != null) {
            return false;
        }

        if (job != null ? !job.equals(that.job) : that.job != null) {
            return false;
        }

        if (key != null ? !key.equals(that.key) : that.key != null) {
            return false;
        }

        if (plan != null ? !plan.equals(that.plan) : that.plan != null) {
            return false;
        }

        if (task != null ? !task.equals(that.task) : that.task != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = plan != null ? plan.hashCode() : 0;
        result = 31 * result + (job != null ? job.hashCode() : 0);
        result = 31 * result + (task != null ? task.hashCode() : 0);
        result = 31 * result + (item != null ? item.hashCode() : 0);
        result = 31 * result + (key != null ? key.hashCode() : 0);
        return result;
    }
}
