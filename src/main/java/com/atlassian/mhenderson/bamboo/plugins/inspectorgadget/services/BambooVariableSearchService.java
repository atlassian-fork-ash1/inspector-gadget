package com.atlassian.mhenderson.bamboo.plugins.inspectorgadget.services;

import com.atlassian.bamboo.variable.VariableDefinition;
import com.atlassian.mhenderson.bamboo.plugins.inspectorgadget.rest.BambooVariable;

import java.util.List;


/**
 * Provides methods for looking up bamboo variables by name, id, getting all variables
 * for a plan etc.
 */
public interface BambooVariableSearchService {

    /**
     * Lookup a global variable by key.
     * @param variableKey
     * @return The variable definition or null, if not found.
     */
    VariableDefinition lookupGlobalVariableByName(String variableKey);

    /**
     * Lookup a plan variable by key.
     * @param variableKey
     * @return The variable definition or null, if not found.
     */
    VariableDefinition lookupPlanVariableByName(String planAndJobKey, String variableKey);

    /**
     * Lookup a global variable by id.
     * @param variableId
     * @return The variable definition or null, if not found.
     */
    VariableDefinition lookupVariableById(String variableId);

    /**
     * Prepare a full list of variables to be returned.
     * @param variableDefinitions
     * @return List of BambooVariable data objects.
     */
    List<BambooVariable> prepareFullListOfVariables(List<VariableDefinition> variableDefinitions);

    /**
     * Get all variable definitions available to a plan.
     * @param planAndJobKey
     * @return The list of variables.
     */
    List<VariableDefinition> getAllVariableDefinitions(String planAndJobKey);

    /**
     * Get all global variables.
     * @return The list of global variables.
     */
    List<VariableDefinition> getGlobalVariables();

    /**
     * Get all plan variables safely, i.e. swalling any exceptions that occur during the lookup process.
     * @param planAndJobKey
     * @return The list of variables found.
     */
    List<VariableDefinition> getPlanVariablesSafely(String planAndJobKey);

    /**
     * Get the list of previous build runtime variable evaluations.
     * @param planAndJobKey
     */
    void getPreviousBuildRuntimeVariables(String planAndJobKey);

    /**
     * Find a variable by key in the supplied list.
     * @param key
     * @param definitions
     * @return The variable if found, or null.
     */
    VariableDefinition findVariableByKey(String key, List<VariableDefinition> definitions);

    /**
     * Obscure a password key if needed.
     * @param key
     * @param value
     * @return The obscured key if it was a password key.
     */
    String obscurePasswordForKeyIfNecessary(String key, String value);
}
