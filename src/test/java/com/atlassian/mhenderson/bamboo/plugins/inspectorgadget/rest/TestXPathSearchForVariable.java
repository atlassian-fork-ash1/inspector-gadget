package com.atlassian.mhenderson.bamboo.plugins.inspectorgadget.rest;

import com.atlassian.bamboo.build.BuildDefinitionForBuild;
import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.TopLevelPlan;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.mhenderson.bamboo.plugins.inspectorgadget.services.TaskDefinitionBambooPlanSearchService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Tests for the xpath search
 */
@RunWith(MockitoJUnitRunner.class)
public class TestXPathSearchForVariable {

    private TaskDefinitionBambooPlanSearchService searchService;
    private List<ImmutablePlan> plans = new ArrayList<ImmutablePlan>();
    private List<Job> jobs = new ArrayList<Job>();

    @Mock
    private PlanManager oldPlanManager;

    @Mock
    private CachedPlanManager planManager;

    @Mock
    private TopLevelPlan plan;

    @Mock
    private Job job;

    @Mock
    private BuildDefinitionForBuild buildXml;

    @Before
    public void setup() {
        searchService = new TaskDefinitionBambooPlanSearchService(planManager);

        plans.add(plan);
        jobs.add(job);

        when(planManager.getPlans(ImmutablePlan.class)).thenReturn(plans);
        when(plan.getAllJobs()).thenReturn(jobs);
        when(job.getBuildDefinitionXml()).thenReturn(buildXml);
        when(plan.getName()).thenReturn("TEST");
        when(job.getName()).thenReturn("Banana");
        when(job.getKey()).thenReturn("TSTB");
    }

    @Test
    public void doNothing() {
        assertTrue(true);
    }

    //@Test
    public void shouldFindVariableInXmlUsingXPathLookup() throws Exception {
        when(buildXml.getXmlData()).thenReturn(jobXml);

        List<VariableUsageInPlanInformation> results = searchService.findUsagesOfVariableInAllPlans("bamboo.new.variable");

        assertEquals("Should have got 3 nodes.", 3, results.size());
    }

    //@Test
    public void shouldExtractTaskNameFromXmlWhenKeyFound() throws Exception {
        when(buildXml.getXmlData()).thenReturn(jobXml);

        List<VariableUsageInPlanInformation> results = searchService.findUsagesOfVariableInAllPlans("bamboo.new.variable");
        for (VariableUsageInPlanInformation info : results) {
            assertEquals("user description should be 'call command'", "call command", info.getTask());
        }
    }

    //@Test
    public void shouldNotExtractTaskNameFromXmlWhenKeyFound() throws Exception {
        when(buildXml.getXmlData()).thenReturn(jobXmlNoUserDescription);

        List<VariableUsageInPlanInformation> results = searchService.findUsagesOfVariableInAllPlans("bamboo.new.variable");
        for (VariableUsageInPlanInformation info : results) {
            assertEquals("user description should be empty", "", info.getTask());
        }
    }

    //@Test
    public void shouldExtractItemNameFromXmlWhenKeyFound() throws Exception {

        when(buildXml.getXmlData()).thenReturn(jobXml);

        List<VariableUsageInPlanInformation> results = searchService.findUsagesOfVariableInAllPlans("bamboo.new.variable");
        VariableUsageInPlanInformation info = results.get(0);
        assertEquals("usage of variable should be in 'argument'", "argument", info.getItem());
    }

    // Used in PLAN: Blah, JOB:Blah, TASK:Call Command, ITEM:
    private String jobXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
            "<configuration>\n" +
            "  <isMergedConfiguration>false</isMergedConfiguration>\n" +
            "  <cleanWorkingDirectory>false</cleanWorkingDirectory>\n" +
            "  <repositoryDefiningWorkingDirectory>-1</repositoryDefiningWorkingDirectory>\n" +
            "  <buildTasks>\n" +
            "    <taskDefinition>\n" +
            "      <id>1</id>\n" +
            "      <userDescription>call command</userDescription>\n" +
            "      <isEnabled>true</isEnabled>\n" +
            "      <pluginKey>com.atlassian.bamboo.plugins.scripttask:task.builder.command</pluginKey>\n" +
            "      <finalising>false</finalising>\n" +
            "      <rootDirectoryType>INHERITED</rootDirectoryType>\n" +
            "      <repositoryDefiningWorkingDir>-1</repositoryDefiningWorkingDir>\n" +
            "      <config>\n" +
            "        <item>\n" +
            "          <key>argument</key>\n" +
            "          <value>${bamboo.new.variable}</value>\n" +
            "        </item>\n" +
            "        <item>\n" +
            "          <key>label</key>\n" +
            "          <value>blah</value>\n" +
            "        </item>\n" +
            "        <item>\n" +
            "          <key>workingSubDirectory</key>\n" +
            "          <value>${bamboo.new.variable}</value>\n" +
            "        </item>\n" +
            "        <item>\n" +
            "          <key>environmentVariables</key>\n" +
            "          <value>${bamboo.new.variable}</value>\n" +
            "        </item>\n" +
            "      </config>\n" +
            "    </taskDefinition>\n" +
            "  </buildTasks>\n" +
            "  <buildStrategies>\n" +
            "    <defined>true</defined>\n" +
            "  </buildStrategies>\n" +
            "  <branches>\n" +
            "    <monitoringEnabled>false</monitoringEnabled>\n" +
            "    <matchingPattern/>\n" +
            "    <inactivityInDays>30</inactivityInDays>\n" +
            "    <defaultNotificationStrategy>notifyCommitters</defaultNotificationStrategy>\n" +
            "  </branches>\n" +
            "  <branches>\n" +
            "    <defaultBranchIntegration>\n" +
            "      <enabled>false</enabled>\n" +
            "    </defaultBranchIntegration>\n" +
            "    <issueLinking>true</issueLinking>\n" +
            "  </branches>\n" +
            "  <branchIntegration>\n" +
            "    <enabled>false</enabled>\n" +
            "  </branchIntegration>\n" +
            "  <branchConfiguration>\n" +
            "    <cleanup>\n" +
            "      <disabled>false</disabled>\n" +
            "    </cleanup>\n" +
            "    <notificationStrategy>notifyCommitters</notificationStrategy>\n" +
            "  </branchConfiguration>\n" +
            "</configuration>\n";

    private String jobXmlNoUserDescription = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
            "<configuration>\n" +
            "  <isMergedConfiguration>false</isMergedConfiguration>\n" +
            "  <cleanWorkingDirectory>false</cleanWorkingDirectory>\n" +
            "  <repositoryDefiningWorkingDirectory>-1</repositoryDefiningWorkingDirectory>\n" +
            "  <buildTasks>\n" +
            "    <taskDefinition>\n" +
            "      <id>1</id>\n" +
            "      <userDescription/>\n" +
            "      <isEnabled>true</isEnabled>\n" +
            "      <pluginKey>com.atlassian.bamboo.plugins.scripttask:task.builder.command</pluginKey>\n" +
            "      <finalising>false</finalising>\n" +
            "      <rootDirectoryType>INHERITED</rootDirectoryType>\n" +
            "      <repositoryDefiningWorkingDir>-1</repositoryDefiningWorkingDir>\n" +
            "      <config>\n" +
            "        <item>\n" +
            "          <key>argument</key>\n" +
            "          <value>${bamboo.new.variable}</value>\n" +
            "        </item>\n" +
            "        <item>\n" +
            "          <key>label</key>\n" +
            "          <value>blah</value>\n" +
            "        </item>\n" +
            "        <item>\n" +
            "          <key>workingSubDirectory</key>\n" +
            "          <value>${bamboo.new.variable}</value>\n" +
            "        </item>\n" +
            "        <item>\n" +
            "          <key>environmentVariables</key>\n" +
            "          <value>${bamboo.new.variable}</value>\n" +
            "        </item>\n" +
            "      </config>\n" +
            "    </taskDefinition>\n" +
            "  </buildTasks>\n" +
            "  <buildStrategies>\n" +
            "    <defined>true</defined>\n" +
            "  </buildStrategies>\n" +
            "  <branches>\n" +
            "    <monitoringEnabled>false</monitoringEnabled>\n" +
            "    <matchingPattern/>\n" +
            "    <inactivityInDays>30</inactivityInDays>\n" +
            "    <defaultNotificationStrategy>notifyCommitters</defaultNotificationStrategy>\n" +
            "  </branches>\n" +
            "  <branches>\n" +
            "    <defaultBranchIntegration>\n" +
            "      <enabled>false</enabled>\n" +
            "    </defaultBranchIntegration>\n" +
            "    <issueLinking>true</issueLinking>\n" +
            "  </branches>\n" +
            "  <branchIntegration>\n" +
            "    <enabled>false</enabled>\n" +
            "  </branchIntegration>\n" +
            "  <branchConfiguration>\n" +
            "    <cleanup>\n" +
            "      <disabled>false</disabled>\n" +
            "    </cleanup>\n" +
            "    <notificationStrategy>notifyCommitters</notificationStrategy>\n" +
            "  </branchConfiguration>\n" +
            "</configuration>\n";
}
